import { useEffect } from "react";
import Table from 'react-bootstrap/Table';
import { useState } from "react"; 
import Sudoku from "../components/Sudoku/Sudoku";



const SecondLab = () => {
    const [data, setData] = useState([]);

    useEffect(() => {
        fetch('https://dummyjson.com/products?limit=7&skip=10&select=title,price')
          .then((res) => res.json())
          .then((result) => {
            setData(result.products); 
          })
          .catch((error) => { 
            console.error('Error fetching data:', error);
          });
      }, []); 

    return (
        <>
          <div>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {data.map((item, index) => (
                        <tr key={index}>
                            <td>{item.title}</td>
                            <td>{item.price}</td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        </div>
        <div> 
            <Sudoku></Sudoku>
        </div>
        </>
       
    );
}
export default SecondLab;