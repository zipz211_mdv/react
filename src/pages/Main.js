import { Link } from "react-router-dom";

const Main = ()=>{
    return(
        <>
        <Link to='first'>LAB 1</Link>
        <p></p>
        <Link to='second'>LAB 2</Link>
        <p></p>
        <Link to='three'>LAB 3</Link>
        </>
    );
}

export default Main;