import GradeTable from '../components/GradeTable'
import MathOperation from '../components/MathOperation'
import CustomComponent from '../components/CustomComponent'

const FirstLab = () => {
    const grades = [90, 85, 75, 95, 88]; 

    return (
        <div className="App">
            <GradeTable grades={grades} />
            <MathOperation num={10} />
            <CustomComponent />
        </div>
    );
}
export default FirstLab;