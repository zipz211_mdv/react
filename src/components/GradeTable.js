import React from 'react';

const GradeTable = ({ grades }) => {
  const average = grades.reduce((acc, grade) => acc + grade, 0) / grades.length;

  return (
    <div>
      <table>
        <thead>
          <tr>
            <th>Оцінка</th>
          </tr>
        </thead>
        <tbody>
          {grades.map((grade, index) => (
            <tr key={index}>
              <td>{grade}</td>
            </tr>
          ))}
        </tbody>
        <tfoot>
          <tr>
            <td>Середній бал: {average.toFixed(2)}</td>
          </tr>
        </tfoot>
      </table>
    </div>
  );
};

export default GradeTable;