import React, { useState } from 'react';
import './Sudoku.css'; 

const Sudoku = () => {
    const [grid, setGrid] = useState([
        [0, 0, 2, 0, 5, 0, 0, 0, 0],
        [0, 0, 0, 0, 3, 0, 0, 0, 7],
        [0, 0, 0, 0, 0, 0, 0, 4, 0],
        [0, 0, 1, 8, 0, 0, 0, 0, 0],
        [0, 2, 0, 0, 0, 0, 0, 9, 0],
        [0, 0, 0, 7, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 9, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 6, 0],
        [5, 0, 0, 0, 0, 0, 0, 0, 0]
    ]);
    const [selectedCell, setSelectedCell] = useState(null);

    const handleCellClick = (row, col) => {
        setSelectedCell({ row, col });
    };

    const renderGrid = () => {
        return (
            <table className="sudoku-grid">
                <tbody>
                    {grid.map((row, rowIndex) => (
                        <tr key={rowIndex}>
                            {row.map((cell, colIndex) => (
                                <td
                                    key={colIndex}
                                    onClick={() => handleCellClick(rowIndex, colIndex)}
                                    className={selectedCell?.row === rowIndex && selectedCell?.col === colIndex ? 'selected' : ''}
                                >
                                    {cell === 0 ? '' : cell}
                                </td>
                            ))}
                        </tr>
                    ))}
                </tbody>
            </table>
        );
    };

    return (
        <div>
            {renderGrid()}
        </div>
    );
};

export default Sudoku;
