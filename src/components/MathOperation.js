import React from 'react';

const MathOperation = ({num}) => {
  const result = Math.sqrt(num);

  return (
    <div>
      <p>Число: {num}</p>
      <p>Результат операції: {result}</p>
    </div>
  );
};

export default MathOperation;