import Button from 'react-bootstrap/Button';
import { Form, Modal } from "react-bootstrap";
import { useState } from "react";
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'

const ContactUs = () => {
    const [validated, setValidated] = useState(false);
    const [phone, setPhone] = useState("");
    const [name, setName] = useState("");
    const [surname, setSurname] = useState("");
    const [email, setEmail] = useState("");
    const [showModal, setShowModal] = useState(false);
    const handleShowModal = () => setShowModal(true);
    const handleCloseModal = () => setShowModal(false);
    const handleSubmit = async (event) => {
        const form = event.currentTarget;
        event.preventDefault();
        event.stopPropagation();
        if (form.checkValidity() === false) {
            setValidated(true);
            return;
        }
        handleShowModal();
    }
    console.log(phone);
    return (
        <>
            <Form noValidate validated={validated} onSubmit={handleSubmit} style={{ margin: "0 auto" }}>
                <Form.Group>
                    <Form.Label>Name</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter name"
                        name="name"
                        required
                        onChange={(event) => setName(event.target.value)}
                    />
                    <Form.Control.Feedback type="invalid">
                        Please enter title.
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Surname</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter surname"
                        name="surname"
                        required
                        onChange={(event) => setSurname(event.target.value)}
                    />
                    <Form.Control.Feedback type="invalid">
                        Please enter title.
                    </Form.Control.Feedback>
                </Form.Group>
                <PhoneInput
                    required
                    country={'ua'}
                    onChange={(event) => setPhone(event)}
                />
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" required onChange={(event) => setEmail(event.target.value)} />
                    <Form.Control.Feedback type="invalid">
                        Please enter title.
                    </Form.Control.Feedback>
                </Form.Group>
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
            <Modal show={showModal} onHide={handleCloseModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Information</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h1>{phone}</h1>
                    <h1>{name}</h1>
                    <h1>{surname}</h1>
                    <h1>{email}</h1>
                    <h2>Микитюк Дмитро</h2>
                    <h2>ЗІПЗ-21-1</h2>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={handleCloseModal}>
                        Close
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}
export default ContactUs;