import React, { useState } from 'react';

const CustomComponent = () => {
  const [count, setCount] = useState(0);

  const handleIncrement = () => {
    setCount(count + 1);
  };

  return (
    <div>
      <p>Лічильник: {count}</p>
      <button onClick={handleIncrement}>Збільшити</button>
    </div>
  );
};

export default CustomComponent;