import React from 'react';
import './App.css'; 
import { Route, Routes } from 'react-router-dom';
import FirstLab from './pages/FirstLab';
import Main from './pages/Main';
import SecondLab from './pages/SecondLab';
import ThreeLab from './pages/ThreeLab';
import 'bootstrap/dist/css/bootstrap.min.css'; 

function App() {
  
  return (
    <Routes>
      <Route path='/' element={<Main></Main>}></Route>
      <Route path='/first' element={<FirstLab></FirstLab>}></Route>
      <Route path='/second' element={<SecondLab></SecondLab>}></Route>
      <Route path='/three' element={<ThreeLab></ThreeLab>}></Route>
    </Routes> 
  );
}

export default App;